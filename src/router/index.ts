import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [

    {
      path: '/',
      redirect: '/login'
    },
    {
      path: '/login',
      name: 'login',
      component: () => import("@/views/user/Login.vue")
    },
    {
      path: '/user',
      component: () => import("@/views/home/Index.vue"),
      children: [
        {
          path: '',
          name: 'welcome',
          component: () => import("@/views/covid-19/index.vue")
        },
        {
          path: 'activity',
          name: 'activity',
          component: () => import("@/views/activity/Index.vue")
        },
        {
          path: 'chat',
          name: 'chat',
          component: () => import("@/views/chat/Index.vue")
        },
      ]
    },
    {
      path: '/house',
      name: 'house',
      component: () => import("@/views/house/Index.vue")
    },
    {
      path: '/world',
      name: 'world',
      component: () => import("@/views/world/Index.vue")
    },

  ]
})


export default router
