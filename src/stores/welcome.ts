import { defineStore } from 'pinia'
import { getList } from '../api/welcome';

export const welcomeStore = defineStore({
    id: 'welcome',
    state: () => ({
        list: []
    }),
    getters: {
        // doubleCount: (state) => state.counter * 2
    },
    actions: {
        async getList(params: object) {
            let res = await getList(params);
            this.list = res;
        }
    }
})
