import { createApp } from 'vue'
import { createPinia } from 'pinia'

import App from './App.vue'
import router from './router'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'

const app = createApp(App)

app.use(createPinia())
app.use(router)
app.use(ElementPlus)

const whiteList = ['/login']
router.beforeEach((to, from, next) => {
    if (whiteList.includes(to.path)|| localStorage.getItem('token')) {
        next()
    } else {
        next('/login')
    }
})

app.mount('#app')
