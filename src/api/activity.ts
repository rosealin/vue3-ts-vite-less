import { _get } from '@/utils/manage'

export const getActivity = (params?: object) => {
    return _get(`account/activity/activity_list/`, params)
}