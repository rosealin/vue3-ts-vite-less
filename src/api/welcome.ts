import axios from 'axios'
const baseURL = 'http://localhost:3333'

export const getList = () => axios.get(`${baseURL}/api/list`).then(res => res.data)