export const rawSeries = [
    {
        "name": "MAX(num)",
        "id": "MAX(num)",
        "data": [
            {
                "value": -0.32,
                "label": {
                    "position": "right"
                },
                "itemStyle": {}
            },
            {
                "value": 0.34,
                "itemStyle": {}
            },
            {
                "value": -2,
                "label": {
                    "position": "right"
                },
                "itemStyle": {}
            },
            {
                "value": 5,
                "itemStyle": {}
            },
            {
                "value": -1,
                "label": {
                    "position": "right"
                },
                "itemStyle": {}
            },
            {
                "value": 2,
                "itemStyle": {}
            }
        ]
    },
    {
        "name": "MIN(grade)",
        "id": "MIN(grade)",
        "data": [
            {
                "value": 2,
                "label": {
                    "position": "right"
                },
                "itemStyle": {}
            },
            {
                "value": 3,
                "itemStyle": {}
            },
            {
                "value": -2,
                "label": {
                    "position": "right"
                },
                "itemStyle": {}
            },
            {
                "value": -4,
                "itemStyle": {}
            },
            {
                "value": -1,
                "label": {
                    "position": "right"
                },
                "itemStyle": {}
            },
            {
                "value": 1,
                "itemStyle": {}
            }
        ]
    },
    {
        "name": "COUNT(school)",
        "id": "COUNT(school)",
        "data": [
            {
                "value": 1,
                "label": {
                    "position": "right"
                },
                "itemStyle": {}
            },
            {
                "value": 1,
                "itemStyle": {}
            },
            {
                "value": 1,
                "label": {
                    "position": "right"
                },
                "itemStyle": {}
            },
            {
                "value": 2,
                "itemStyle": {}
            },
            {
                "value": 1,
                "label": {
                    "position": "right"
                },
                "itemStyle": {}
            },
            {
                "value": 2,
                "itemStyle": {}
            }
        ]
    }
]