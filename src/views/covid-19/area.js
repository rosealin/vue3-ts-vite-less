export const data = [
    {
      name: "内蒙古",
      itemStyle: {
        areaColor: "#56b1da"
      },
      value: 110.3467
    },
    {
      name: "西藏",
      itemStyle: {
        areaColor: "#56b1da"
      },
      value: 60.3467
    },
    {
      name: "新疆",
      itemStyle: {
        areaColor: "#56b1da"
      },
      value: 30.3467
    },
    {
      name: "青海",
      itemStyle: {
        areaColor: "#56b1da"
      },
      value: 20.3467
    },
    {
      name: "四川",
      itemStyle: {
        areaColor: "#56b1da"
      },
      value: 70.3467
    }
  ];