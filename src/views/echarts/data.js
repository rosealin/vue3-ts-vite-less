const dataTool = () => {
         // 写一个通用的获取单例的函数 , fn 为执行单次的函数
         let getSingle = function ( fn ) {
            let result;
            return function () {
                return result || ( result = fn.apply(this, arguments))
            }
        }
        // 绑定事件
        let bindEvent = function () {
            document.getElementById('app').addEventListener('click',()=>{
                console.log('我出现了')
            });
        }();
        // 无论渲染多少次，事件只绑定一次
        let render = function () {
            console.log('开始渲染！');  
            getSingle(bindEvent)
        }
        // render();
        // render();
        // render();
};

export default dataTool;
