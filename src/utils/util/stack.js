// stack：based on array
function Stack() {
    this.items = []
        //对象原型上添加方法，对象实例共享
    Stack.prototype.push = function(ele) {
        this.items.push(ele)
    }
    Stack.prototype.pop = function() {
        return this.items.pop()
    }
    Stack.prototype.peek = function() {
        return this.items[this.items.length - 1]
    }
    Stack.prototype.isEmpty = function() {
        return this.items.length == 0
    }
    Stack.prototype.size = function() {
        return this.items.length
    }
    Stack.prototype.toString = function() {
        let res = '';
        for (let i = 0; i < this.items.length; i++) {
            res += this.items[i] + ' ';
        }
        return res;
    }
}
let stack = new Stack();
export default stack;