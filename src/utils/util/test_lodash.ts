import { isEqual, omitBy, pickBy, isUndefined, isNull, isNumber } from 'lodash';
import { areObjectsEqual } from '@util/tools'

export const test = () => {

    var object = { 'a': 1, 'b': '2', 'c': 3 };
    console.log("object",object)
    console.log("pickBy isNumber",pickBy(object, isNumber));
    console.log("omitBy isNumber",omitBy(object, isNumber));


    let obj1 = { 'a': 3, 'b': '2', 'c': 3, 'd': undefined }
    console.log("obj1",obj1)
    console.log("pickBy isUndefined",pickBy(obj1, isUndefined));
    console.log("omitBy isUndefined",omitBy(obj1, isUndefined));

    let obj2 = { 'a': 3, 'b': '2', 'c': 3, 'd': null }
    let flag = areObjectsEqual(obj1, obj2, { ignoreUndefined: true, ignoreNull: true })
    console.log("obj2",obj2)
    console.log('对象是否相等：obj1 is equal to obj2:', flag)

    let obj3 = { 'a': 3, 'b': '2', 'c': 3 }
    let obj4 = { ...obj3 }
    console.log("obj3",obj3)
    console.log("展开运算符实现深拷贝：obj4 = {...obj3 }",obj4)
    obj3.a = 5
    console.log("obj3.a = 5",obj3)
    console.log("obj4",obj4)
}