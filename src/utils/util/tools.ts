import { ref } from 'vue';
import { isEqual, omitBy, isUndefined, isNull, isNumber } from 'lodash';
import stack from './stack'
export const modalBox = {
    setup() {
        const num = ref('1')
        return {
            num
        }
    },
    template: `
    <div>
    <div>{{num}}</div>
    <div>这是一个弹窗</div>
    </div>
    `
}
//十进制转为二进制
export function dec2binary(decNumber) {
    while (decNumber > 0) {
        stack.push(decNumber % 2)
        decNumber = Math.floor(decNumber / 2)
    }
    let binaryString = ''
    while (!stack.isEmpty()) {
        binaryString += stack.pop()
    }
    return binaryString;

}
/**
 * @param {Function} fn 防抖函数
 * @param {Number} delay 延迟时间
 */
export function debounce(fn, delay) {
    var timer;
    return function () {
        var context = this;
        var args = arguments;
        clearTimeout(timer);
        timer = setTimeout(function () {
            fn.apply(context, args);
        }, delay);
    };
}

export function areObjectsEqual(
    obj1: any,
    obj2: any,
    opts: {
        ignoreUndefined?: boolean;
        ignoreNull?: boolean;
    } = { ignoreUndefined: false, ignoreNull: false },
) {
    let comp1 = obj1;
    let comp2 = obj2;

    if (opts.ignoreUndefined) {
        comp1 = omitBy(comp1, isUndefined);
        comp2 = omitBy(comp2, isUndefined);
    }
    if (opts.ignoreNull) {
        comp1 = omitBy(comp1, isNull);
        comp2 = omitBy(comp2, isNull);
    }
    return isEqual(comp1, comp2);
}
// to find obj from arr object according id
export function getFromArr(arr: Record<string, any>[], id: string) {
    let obj;
    arr.forEach(o => {
        if (o.id === id) {
            obj = o;
        }
    });
    return obj;
}

//reduce -> to find the number of the string in array
export function getStrNumFromArr(str: string) {
    let arr = str.split('');
    return arr.reduce((all: Record<string, number>, current: string) => {
        if (current in all) {
            all[current]++
        } else {
            all[current] = 1
        }
        return all
    }, {})
}


export function processArr(rawSeries) {
    let arr = [];
    for (let i = rawSeries[0].data.length - 1; i > -1; i--) {
        arr[i] = [];
        for (let j = rawSeries.length - 1; j > -1; j--) {
            if (rawSeries[j].data[i].value != 0 && rawSeries[j].data[i].value > 0) {
                arr[i].push(true)
            } else {
                arr[i].push(false);
            }
        }
    }
    return arr;
}

export function styledData(data) {
    let temp = data.map((item, index) => {
        if (item.value > 0) {
            return {
                ...item,
                itemStyle: {
                    borderRadius: [0, 10, 10, 0]
                },
            }
        } else {
            return {
                ...item,
                itemStyle: {
                    borderRadius: [10, 0, 0, 10]
                },
            }
        }

    })
    console.log(temp)
    return temp;
}

export function styleBorderRadius(rawSeries) {
    let temp = rawSeries.map((series, idx) => {
        if (idx == rawSeries.length - 1) {
            return {
                ...series,
                data: series.data.map((item, index) => {
                    if (item.value && item.value > 0) {
                        return {
                            ...item,
                            itemStyle: {
                                borderRadius: [0, 10, 10, 0]
                            },
                        }
                    } else {
                        return {
                            ...item,
                            itemStyle: {
                                borderRadius: [10, 0, 0, 10]
                            },
                        }
                    }
                })
            }
        } else {
            return { ...series };
        }
    })
    console.log(temp)
    return temp;
}