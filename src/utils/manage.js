import request from '@/utils/http'

//post
export function _post(url, parameter) {
    return request({
        url: url,
        method: 'post',
        data: parameter
    })
}

//post method= {post | put}
export function httpAction(url, parameter, method) {
    return request({
        url: url,
        method: method,
        data: parameter
    })
}

//put
export function _put(url, parameter) {
    return request({
        url: url,
        method: 'put',
        data: parameter
    })
}

//get
export function _get(url, parameter) {
    return request({
        url: url,
        method: 'get',
        params: parameter
    })
}

//deleteAction
export function _delete(url, parameter) {
    return request({
        url: url,
        method: 'delete',
        params: parameter
    })
}