import axios from 'axios'

import { ElMessage ,ElLoading  } from 'element-plus';

const baseUrl = (window as any).g.BASEURL


const request = axios.create({
    baseURL: baseUrl,  
    headers: {
        'Content-Type': 'application/json;charset=UTF-8',
    },
    timeout: 500000, // 请求超时时间
})

let loading = null //定义loading变量

//开始 加载loading
let startLoading = () => {
    loading = ElLoading.service({
        lock: true,
        text: '加载中……',
        background: 'rgba(0, 0, 0, 0.7)'
    })
}
//结束 取消loading加载
let endLoading = () => {
    loading.close()
}

//showFullScreenLoading() 与 tryHideFullScreenLoading() 目的是合并同一页面多个请求触发loading

let needLoadingRequestCount = 0 //声明一个变量

let showFullScreenLoading = () => {
    if (needLoadingRequestCount === 0) { //当等于0时证明第一次请求 这时开启loading
        startLoading()
    }
    needLoadingRequestCount++ //全局变量值++ 
}

let tryHideFullScreenLoading = () => {
    if (needLoadingRequestCount <= 0) return //小于等于0 证明没有开启loading 此时return
    needLoadingRequestCount-- //正常响应后 全局变量 --
    if (needLoadingRequestCount === 0) {  //等于0 时证明全部加载完毕 此时结束loading 加载
        endLoading()
    }
}

request.interceptors.request.use(
    config => {
        //开启loading加载
        showFullScreenLoading();
        const token = localStorage.getItem('token');
        if (token) {
            // 判断是否存在token，如果存在的话，则每个http header都加上token
            config.headers.authorization = token; //请求头加上token
        }
        return config;
    },
    error => {
        // 关闭loading
        tryHideFullScreenLoading();
        return Promise.reject(error);
    }
);

request.interceptors.response.use(
    response => {
        //关闭loading加载
        tryHideFullScreenLoading()
        if (response.status == 200) {
            return Promise.resolve(response);
        } else {
            handleErrorData(response.data);
            return Promise.reject(response);
        }
    },
    error => {
        // 关闭loading
        tryHideFullScreenLoading();
        return Promise.reject(error);
        // alert(JSON.stringify(error), '请求异常', {
        //     confirmButtonText: '确定',
        //     callback: (action) => {
        //         console.log(action)
        //     }
        // });
    }
);

//对错误信息的处理函数
function handleErrorData(errMes) {
    if (errMes.message) {
        ElMessage.error(errMes.message);
    } else {
        switch (errMes.code) {
            case 401:
                ElMessage.error("未授权，请重新登录!");
                break;
            case 403:
                ElMessage.error("拒绝访问");
                break;
            case 404:
                ElMessage.error("很抱歉，资源未找到!");
                break;
            case 500:
                ElMessage.error("服务器错误!");
                break;
            case 504:
                ElMessage.error("网络超时!");
                break;
            default:
                ElMessage.error("服务正在联调中，请稍后!");
                break;
        }
    }
}
export default request; 